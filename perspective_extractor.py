# Importações
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import nltk
nltk.download('wordnet')
nltk.download('omw-1.4')
from nltk.corpus import wordnet as wn
import spacy
import re
import matplotlib.pyplot as plt

df_queries = pd.read_csv('dataset_query_category.csv', sep=',')
df_queries = df_queries.reset_index()  # Garantir que os índices batem com a quantidade de linhas
queries = []
for index, row in df_queries.iterrows():
    queries.append(row['query'])

# criando dataframe vazio
df_dmoz = pd.DataFrame(columns=['category', 'title', 'desc'])
lines=[]

# LENDO DMOZ ODP
with open('content.rdf.u8', 'r') as fl_in:
    lines = fl_in.readlines()

lines = [str(line) for line in lines]
# extract titles, sescriptions, and topics
titles = [re.findall('<d:Title>(.+)</d:Title>', line) for line in lines]
descs = [re.findall('<d:Description>(.+)</d:Description>', line) for line in lines]
topics = [re.findall('<topic>(.+)</topic>', line) for line in lines]
del lines

titles_pos = [i for i, x in enumerate(titles) if len(x)>0]
descs_pos = [i for i, x in enumerate(descs) if len(x)>0]
topics_pos = [i for i, x in enumerate(topics) if len(x)>0]

print('# titles found: '+str(len(titles_pos)))
print('# descriptions found: '+str(len(descs_pos)))
print('# topics found: '+str(len(topics_pos)))

topics_list = []
titles_list = []
descs_list = []
# Filtrando para o datafram ter apenas dados completos
for line_counter, i in enumerate(titles_pos):
    if line_counter % 100000 == 0:
        print(line_counter,'processed')
    if (i+1) != len(descs) and (i+2) != len(topics):
        if len(descs[i+1])>0 and len(topics[i+2])>0:
            topics_list.append(topics[i+2][0].split('/')[1])
            titles_list.append((titles[i][0]))
            descs_list.append((descs[i+1][0]))

# Acrescentando ao dataframe criado anteriormente
df_dmoz.category = topics_list
df_dmoz.title = titles_list
df_dmoz.desc = descs_list
print(df_dmoz.shape)
#print(df_dmoz.head())
#print()
#print(df_dmoz.tail())
#print(df_dmoz.category.value_counts())

fig = plt.figure(figsize=(12, 5))
df_dmoz.category.value_counts().plot(kind='bar')
plt.title('Category counts')
fig.savefig('category_counts.png', dpi=fig.dpi)

# Na figura é possível observar a diferença na presença das categorias World e Regional, 
# como tem muito mais do que as outras categorias e tem diferentes línguas, mais atrapalha do que ajuda, então irei tirá-las

print('df shape before', df_dmoz.shape)
df_dmoz = df_dmoz[~df_dmoz.category.isin(['World', 'Regional'])]
print('df shape after', df_dmoz.shape)

fig = plt.figure(figsize=(12, 5))
df_dmoz.category.value_counts().plot(kind='bar')
plt.title('Category counts')
fig.savefig('new_category_counts.png', dpi=fig.dpi)

# Juntando o título com a descrição para que a pesquisa retorne mais categorias possíveis
#df_dmoz.desc = df_dmoz.title + ' ' + df_dmoz.desc
#df_dmoz = df_dmoz.drop(['title'], axis=1)
df_dmoz.desc = df_dmoz.desc.str.lower()
df_dmoz.title = df_dmoz.title.str.lower()

nlp = spacy.load('en_core_web_sm')

acertos = 0
# Percorrendo as queries
linha = 0
for index, row in df_queries.iterrows():
    query = row['query']
    doc = nlp(query)

    # Pego as palavras-chave da consulta realizada
    lista_de_termos = [token.orth_.lower() for token in doc if not token.is_stop and not token.is_punct and not token.is_space]
    #print(lista_de_termos)
    df = df_dmoz[df_dmoz['title'].str.contains('|'.join(lista_de_termos))]
    # Verifico na descrição
    if len(df) == 0:
       df = df_dmoz[df_dmoz['desc'].str.contains('|'.join(lista_de_termos))]

    # Pego os sinônimos de cada uma das palavras-chave
    '''
    for termo in lista_de_termos:
        # Procuro com os termos apenas

        # Procuro com os sinônimos
        sinonimos = wn.synsets(termo)
        lista_sinonimos = []
        for sinonimo in sinonimos:
            sinonimo = sinonimo.name()
            aux = sinonimo.split('.')
            sinonimo_nome = aux[0]
            # Como há vários significados diferentes, é retornado vários sinônimos que são a mesma palavra, 
            # então estou filtrando para adicionar apenas o que já não está presente na lista
            if sinonimo_nome not in lista_sinonimos:
                lista_sinonimos.append(sinonimo_nome)
            print(f"Os sinonimos encontrados para a palavra-chave {termo} sao: {lista_sinonimos}")
    '''
    categories = []
    best_category = ''
    best_value = 0
    for index, row in df.iterrows():
        categories.append(row['category'])
    #if len(df) < 4:
    #    print(df['title'])
    categories_unique = list(dict.fromkeys(categories))
    """ print('before: ', str(len(categories)))
    print('after: ', str(len(categories_unique)))
    print(categories_unique)
    print() """
    first = True
    # Se tiver mais de uma categoria, decido a melhor pela quantidade de matchs com os termos
    if len(categories_unique) > 1:
        indice = 0
        for index, line in df.iterrows():
            title_words = line['title'].split(' ')
            categoria = df['category'].iloc[indice]
            cont=0
            for termo in lista_de_termos:
                if termo in title_words:
                    cont+=1
            if first:
                best_value = cont
                best_category = categoria
                first = False
            elif cont > best_value and categoria != best_category:
                best_value = cont
                best_category = categoria
            indice+=1
    elif len(categories_unique) == 1:
        df_unique = df.drop_duplicates(subset=['category'])
        best_category = df_unique['category'].iloc[0]
    else:
        best_category = 'Not Found'
    
    right_category = df_queries['category'].iloc[linha]
    if right_category == best_category:
        acertos+=1
    
    linha+=1


acuracia = acertos/len(queries)
print("*** Resultado ***")
print("Total de acertos: " + str(acertos))
print("Acuracia: " + str(acuracia))